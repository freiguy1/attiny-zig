const std = @import("std");
const microzig = @import("microzig");
const regs = microzig.chip.registers;

pub fn main() !void {
    regs.PORTA.DDRA.* = 0b1;
    var on = false;
    while (true) {
        // regs.PORTA.PINA.* = 0b1;
        if (on) {
            regs.PORTA.PORTA.* = 0b0;
        } else {
            regs.PORTA.PORTA.* = 0b1;
        }
        on = !on;
        delay();
    }
}

fn delay() void {
    var counter: u24 = 0;
    while (counter < 50_000) {
        // var counter2: u8 = 0;
        // while (counter2 < 255) {
        //     counter2 += 1;
        // }
        counter += 1;
        std.mem.doNotOptimizeAway(counter);
    }
}
