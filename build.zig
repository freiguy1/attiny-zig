const std = @import("std");
const microzig = @import("lib/microzig/src/main.zig");

pub fn build(b: *std.build.Builder) void {
    const avr25 = microzig.Cpu{
        .name = "AVR25",
        .path = "src/avr25.zig",
        .target = std.zig.CrossTarget{
            .cpu_arch = .avr,
            .cpu_model = .{ .explicit = &std.Target.avr.cpu.avr25 },
            .os_tag = .freestanding,
            .abi = .eabi,
        },
    };

    const attiny84 = microzig.Chip{
        .name = "ATtiny84",
        .path = "src/attiny44.zig",
        .cpu = avr25,
        .memory_regions = &.{
            .{ .offset = 0x000000, .length = 8 * 1024, .kind = .flash },
            .{ .offset = 0x800060, .length = 512, .kind = .ram },
        },
    };

    const backing = .{
        .chip = attiny84,
    };

    var exe = microzig.addEmbeddedExecutable(
        b,
        "thing.elf",
        "src/main.zig",
        backing,
        .{
            // optional slice of packages that can be imported into your app:
            // .packages = &my_packages,
        },
    );
    exe.setBuildMode(.ReleaseSmall);
    exe.install();

    const bin = exe.installRaw("thing.bin", .{});
    const bin_step = b.step("bin", "Generate binary file to be flashed");
    bin_step.dependOn(&bin.step);

    const flash_cmd = b.addSystemCommand(&[_][]const u8{ "avrdude", "-p", "t84", "-c", "usbtiny", "-U", "flash:w:zig-out/bin/thing.bin" });
    flash_cmd.step.dependOn(&bin.step);
    const flash_step = b.step("flash", "Flash and run app on microcontroller");
    flash_step.dependOn(&flash_cmd.step);
}
