The fuse bits for this program are set to the default:

`avrdude -p t84 -c usbtiny -U lfuse:w:0x62:m -U hfuse:w:0xdf:m -U efuse:w:0xff:m`

This uses internal 8MHz oscillator divided by 8, giving the thing a clock value of 1MHz.
